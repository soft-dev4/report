/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kim.albumproject.poc;

import com.kim.albumproject.model.ReportSale;
import com.kim.albumproject.service.ReportService;
import java.util.List;

/**
 *
 * @author Asus
 */
public class TestReportSale {

    public static void main(String[] args) {
        ReportService reportService = new ReportService();
        
//        List<ReportSale> report = reportService.getReportSaleByDay();
//        for (ReportSale r : report) {
//            System.out.println(r);
//        }
        
        List<ReportSale> reportMonth = reportService.getReportSaleByMonth(2013);
        for (ReportSale r : reportMonth) {
            System.out.println(r);
        }
    }
}
